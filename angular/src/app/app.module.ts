import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { HttpClientModule } from '@angular/common/http';
import { EpaymentsComponent } from './epayments/epayments.component';
import { EpaymentsService } from '../services/epayments.services';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    EpaymentsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    EpaymentsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
