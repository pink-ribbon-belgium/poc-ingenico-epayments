import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EpaymentsComponent } from './epayments/epayments.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

const routes: Routes = [
  {
    path: 'epayments',
    component: EpaymentsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule],
  providers: [
  ]
})
export class AppRoutingModule { }
