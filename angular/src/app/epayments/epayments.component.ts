import { Component, OnInit } from '@angular/core';
import {EpaymentsService} from '../../services/epayments.services';
import Config from '../../config.json';
import axios from 'axios';
import CreatePaymentStub from '../../stubs/payments/create_hosted.json';
import crypto from 'crypto';

@Component({
  selector: 'app-epayments-component',
  templateUrl: './epayments.component.html',
  styleUrls: ['./epayments.component.css']
})
export class EpaymentsComponent implements OnInit {
    paymentResponse: string;
    config = Config;

  constructor(public ePaymentsService: EpaymentsService) { }

  ngOnInit() {
  }

  private createPayment() {
      /* 1. Gather order details in app */

      /* 2. Create payment details */
      // TODO: fill a jsonobject with values yourself
      const paymentStub = CreatePaymentStub;

      /* 3. Initialize connectSdk */
      const paymentDetails = {
          order: {
              orderId: '3',
              currencyCode: 'EUR',
              amount: 10
          },
          customer: {
              customerId: '1234',
              locale: 'nl_BE'
          }
      };

      // Note: Check implementation details on this website:
      // tslint:disable-next-line:max-line-length
      // https://epayments-support.ingenico.com/en/integration/all-sales-channels/integrate-with-e-commerce/guide#template-hosted-by-merchant-(dynamic-template)
      // Note: Test seedString/hash conversion on this website:
      // https://secure.ogone.com/Ncol/Test/testsha.asp
      let seedString = `AMOUNT=${paymentDetails.order.amount}${Config.shaIn}`;
      seedString += `CURRENCY=${paymentDetails.order.currencyCode}${Config.shaIn}`;
      seedString += `LANGUAGE=${paymentDetails.customer.locale}${Config.shaIn}`;
      seedString += `ORDERID=${paymentDetails.order.orderId}${Config.shaIn}`;
      seedString += `PSPID=${Config.pspId}${Config.shaIn}`;
      const shaInHash = crypto.createHash('sha512').update(String(seedString)).digest('hex').toUpperCase();
      console.log(`createPayment.js ::: seedString = ${seedString}`);
      console.log(`createPayment.js ::: shaInHash = ${shaInHash}`);
      // tslint:disable-next-line:max-line-length
      const postUrl = `${Config.postUrl}?AMOUNT=${paymentDetails.order.amount}&CURRENCY=${paymentDetails.order.currencyCode}&LANGUAGE=${paymentDetails.customer.locale}&ORDERID=${paymentDetails.order.orderId}&PSPID=${Config.pspId}&SHASIGN=${shaInHash}`;
      console.log(`createPayment.js ::: postUrl = ${postUrl}`);

      axios.post(postUrl).then(res => {
          console.log(`RESULT: ${JSON.stringify(res)}`);
          console.log(`RESULT.DATA: ${JSON.stringify(res.data)}`);
          this.paymentResponse = res.data;
      })
      .catch(error => {
              console.log(`ERROR: ${JSON.stringify(error)}`);
          });
  }

}
