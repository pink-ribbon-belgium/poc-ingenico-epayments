export interface PaymentDetails {
  receiverId: string;
  amount: {
    amount: number,
    currency: string
  };
}
