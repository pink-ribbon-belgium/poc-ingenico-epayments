import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import CreatePaymentStub from '../stubs/payments/create_hosted.json';
import Config from '../config.json';
import axios from 'axios';

@Injectable({
    providedIn: 'root'
})
export class EpaymentsService {
    // Create public observable of Payment response
    private paymentResponseSubject$ = new BehaviorSubject<any>(null);
    paymentResponse$ = this.paymentResponseSubject$.asObservable();

    constructor() {
    }

    createPayment$(): Observable<any> {
    // public createPayment$() {
    // const logger = logger, // logger undefined = console.log will be used

        /* 1. Gather order details in app */

        /* 2. Create payment details */
        // TODO: fill a jsonobject with values yourself
        const paymentStub = CreatePaymentStub;

        /* 3. Initialize connectSdk */
        const paymentDetails = {
            order: {
                orderId: 3,
                currencyCode: 'EUR',
                amount: 10
            },
            customer: {
                customerId: '1234',
                locale: 'nl_BE'
            }
        };

        // tslint:disable-next-line:max-line-length
        const postUrl = `${Config.postUrl}?amount=${paymentDetails.order.amount}&currency=${paymentDetails.order.currencyCode}&language=${paymentDetails.customer.locale}&orderId=1&pspId=${Config.pspId}&shaSign=${Config.shaIn}`
        console.log(`createPayment.js ::: postUrl = ${postUrl}`);

        axios.post(postUrl).then(res => {
                return res;
            })
            .catch(error => {
                return undefined;
            });
        return undefined;
        /*connectSdk.hostedcheckouts.create(Config.merchantId, paymentStub, null, (error, sdkResponse) => {
          res =  sdkResponse;
        });*/
        // TODO: To go to the payment page, redirect to `https://payment.${sdkResponse.body.partialRedirectUrl}`
}}
